import React from 'react';
import './App.css';


const Button = ({onClick, label}) => {
  return (
    <div>
      <button onClick={onClick}>{label}</button>
    </div>
  );
};

const App = () => {
  return (
    <div className="App">

      <Button
        onClick={ev => {
          ev.preventDefault ();
          console.log ('kkk');
        }}
        label={'Wcisnij mnie'}
      />
    </div>
  );
};

export default App;
