  // w tym komponencie wartosc inputa ma być przechowywana w stanie pod kluczem "inputValue"
  // ten komponent ma mieć metodę onInputChange która loguje zmiany wartości do konsoli
import React, {Component} from 'react';
import './App.css';



export const htmlInput= ({value, onChange}) => {
  return (
    <div>
      <input type="text" value={value} placeholder='Wpisz tekst' onChange={(ev)=>{
        onChange(ev.target.value)
      }}/>
    </div>
    )
  }


export class Input extends Component{
constructor(props){
  super(props)

    this.state = {
      text : ""
    }

    this.onChangeInput = this.onChangeInput.bind(this)
}
 onChangeInput(value){  // bez tego nie wpisuje nic do inputa tylko widac w consoli 
   this.setState({      // setState wbudowana funkcja która pozwala na zmiane stanu reacta aby wartosc z inputa mozna było pokazac userowi 
      'text':value
   })
   console.log(value)
 }  
render(){
  const {text} = this.state
    return (
      <div>

        <htmlInput value={text} onChange={this.onChangeInput}  />
      
      </div>
       
    )   
}
}
const App = () => {
  return (
    <div>
      <Input/>
    </div>
  );
}

export default App;
