import React, {Component} from 'react';
import './App.css';
import axios from 'axios';

const TodoItem = ({title}) => {
  return (
    <li>
      <h3>{title}</h3>
    </li>
  );
};
const TodoLoanding = () => {
  return <h1>Loading list TODO ............</h1>;
};
class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      inBusy: true,
    };
  }

  componentDidMount() {
    axios.get('http://195.181.210.249:3000/todo/').then(response => {
      console.log(response);

      if (response.status === 200) {
        this.setState({
          list: response.data,
          inBusy: false,
        });
      }
    });
  }
  render() {
    let list;
    if (this.state.inBusy) {
      return <TodoLoanding />;
    }

    if (this.state.list.length > 0) {
      list = this.state.list.map(todo => (
        <TodoItem title={todo.title} key={todo.id} />
      ));
      return <ul>{list}</ul>;
    }
    return <h1>List is empty</h1>;
  }
}

const App = () => {
  return (
    <div className="App">
      <List />
    </div>
  );
};

export default App;
