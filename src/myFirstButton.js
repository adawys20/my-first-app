export React from 'react';

// 2. z podanego htmla wytworzyć aktywnie działające komponenty funkcyjne
const htmlButton = ({label, onButtonClick}) => {
    return (
    <div>
        <button>{label}</button>
    </div>)

// ten komponent ma otrzymywać propsy "label" i "onButtonClick"
// "label" ma być wyrenderowany jako label przycisku
// "onButtonClick" ma być przyczepiony do eventu onclick tego buttona oraz ma logować do konsoli string "kliknięto przycisk"
const Button = ()=>{}


