import React from 'react';


export const Table = ({children}) => {
    return (
        <table>
          {children}
        </table>
    );
}
export const TableHead = ()=>{
    return (
        <thead>
            <tr>
                <th>Month</th>
                <th>Savings</th>
            </tr>
        </thead>
  );
}
export const TableBody = ()=>{
    return (
        <tbody>
            <tr>
                <td>January</td>
                <td>$100</td>
            </tr>
            <tr>
                <td>February</td>
                <td>$80</td>
            </tr>
            
            
            
        </tbody>
    );
}
export const TableFoot = ()=>{
    return (
        <tfoot>
            <tr>
                <td>Sum</td>
                <td>$180</td>
            </tr>
        </tfoot>
    )
}

